/* tslint:disable */
import {
  Entrant
} from '../index';

declare var Object: any;
export interface PersonInterface {
  "live"?: boolean;
  "moneroAddress"?: string;
  "electroneumAddress"?: string;
  "throttle"?: number;
  "threads"?: number;
  "hashesPerSecond"?: number;
  "averageHashesPerSecond"?: number;
  "totalHashes"?: number;
  "acceptedHashes"?: number;
  "realm"?: string;
  "username"?: string;
  "email": string;
  "emailVerified"?: boolean;
  "id"?: number;
  "password"?: string;
  accessTokens?: any[];
  entrants?: Entrant[];
}

export class Person implements PersonInterface {
  "live": boolean;
  "moneroAddress": string;
  "electroneumAddress": string;
  "throttle": number;
  "threads": number;
  "hashesPerSecond": number;
  "averageHashesPerSecond": number;
  "totalHashes": number;
  "acceptedHashes": number;
  "realm": string;
  "username": string;
  "email": string;
  "emailVerified": boolean;
  "id": number;
  "password": string;
  accessTokens: any[];
  entrants: Entrant[];
  constructor(data?: PersonInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Person`.
   */
  public static getModelName() {
    return "Person";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Person for dynamic purposes.
  **/
  public static factory(data: PersonInterface): Person{
    return new Person(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Person',
      plural: 'People',
      path: 'People',
      idName: 'id',
      properties: {
        "live": {
          name: 'live',
          type: 'boolean'
        },
        "moneroAddress": {
          name: 'moneroAddress',
          type: 'string'
        },
        "electroneumAddress": {
          name: 'electroneumAddress',
          type: 'string'
        },
        "throttle": {
          name: 'throttle',
          type: 'number'
        },
        "threads": {
          name: 'threads',
          type: 'number'
        },
        "hashesPerSecond": {
          name: 'hashesPerSecond',
          type: 'number'
        },
        "averageHashesPerSecond": {
          name: 'averageHashesPerSecond',
          type: 'number'
        },
        "totalHashes": {
          name: 'totalHashes',
          type: 'number'
        },
        "acceptedHashes": {
          name: 'acceptedHashes',
          type: 'number'
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        entrants: {
          name: 'entrants',
          type: 'Entrant[]',
          model: 'Entrant',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'personId'
        },
      }
    }
  }
}
