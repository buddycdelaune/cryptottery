/* tslint:disable */

declare var Object: any;
export interface BetaUserInterface {
  "email": string;
  "id"?: number;
}

export class BetaUser implements BetaUserInterface {
  "email": string;
  "id": number;
  constructor(data?: BetaUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `BetaUser`.
   */
  public static getModelName() {
    return "BetaUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of BetaUser for dynamic purposes.
  **/
  public static factory(data: BetaUserInterface): BetaUser{
    return new BetaUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'BetaUser',
      plural: 'BetaUsers',
      path: 'BetaUsers',
      idName: 'id',
      properties: {
        "email": {
          name: 'email',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
