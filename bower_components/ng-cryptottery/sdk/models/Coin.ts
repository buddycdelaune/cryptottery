/* tslint:disable */
import {
  Pool
} from '../index';

declare var Object: any;
export interface CoinInterface {
  "name": string;
  "code": string;
  "wallet": string;
  "id"?: number;
  pools?: Pool[];
}

export class Coin implements CoinInterface {
  "name": string;
  "code": string;
  "wallet": string;
  "id": number;
  pools: Pool[];
  constructor(data?: CoinInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Coin`.
   */
  public static getModelName() {
    return "Coin";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Coin for dynamic purposes.
  **/
  public static factory(data: CoinInterface): Coin{
    return new Coin(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Coin',
      plural: 'Coins',
      path: 'Coins',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "code": {
          name: 'code',
          type: 'string'
        },
        "wallet": {
          name: 'wallet',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
        pools: {
          name: 'pools',
          type: 'Pool[]',
          model: 'Pool',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'coinId'
        },
      }
    }
  }
}
