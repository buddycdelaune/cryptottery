/* tslint:disable */
import {
  Coin,
  Entrant
} from '../index';

declare var Object: any;
export interface PoolInterface {
  "percentage": number;
  "interval": number;
  "wallet": string;
  "host": string;
  "port": number;
  "serverPort": number;
  "active": boolean;
  "created"?: Date;
  "coinId": number;
  "displayName": string;
  "id"?: number;
  coin?: Coin;
  entrants?: Entrant[];
}

export class Pool implements PoolInterface {
  "percentage": number;
  "interval": number;
  "wallet": string;
  "host": string;
  "port": number;
  "serverPort": number;
  "active": boolean;
  "created": Date;
  "coinId": number;
  "displayName": string;
  "id": number;
  coin: Coin;
  entrants: Entrant[];
  constructor(data?: PoolInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Pool`.
   */
  public static getModelName() {
    return "Pool";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Pool for dynamic purposes.
  **/
  public static factory(data: PoolInterface): Pool{
    return new Pool(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Pool',
      plural: 'Pools',
      path: 'Pools',
      idName: 'id',
      properties: {
        "percentage": {
          name: 'percentage',
          type: 'number',
          default: 100
        },
        "interval": {
          name: 'interval',
          type: 'number',
          default: 604800
        },
        "wallet": {
          name: 'wallet',
          type: 'string'
        },
        "host": {
          name: 'host',
          type: 'string'
        },
        "port": {
          name: 'port',
          type: 'number'
        },
        "serverPort": {
          name: 'serverPort',
          type: 'number'
        },
        "active": {
          name: 'active',
          type: 'boolean',
          default: true
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "coinId": {
          name: 'coinId',
          type: 'number'
        },
        "displayName": {
          name: 'displayName',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
        coin: {
          name: 'coin',
          type: 'Coin',
          model: 'Coin',
          relationType: 'belongsTo',
                  keyFrom: 'coinId',
          keyTo: 'id'
        },
        entrants: {
          name: 'entrants',
          type: 'Entrant[]',
          model: 'Entrant',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'poolId'
        },
      }
    }
  }
}
