/* tslint:disable */
import { Injectable } from '@angular/core';
import { Person } from '../../models/Person';
import { BetaUser } from '../../models/BetaUser';
import { Pool } from '../../models/Pool';
import { Coin } from '../../models/Coin';
import { Entrant } from '../../models/Entrant';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Person: Person,
    BetaUser: BetaUser,
    Pool: Pool,
    Coin: Coin,
    Entrant: Entrant,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
