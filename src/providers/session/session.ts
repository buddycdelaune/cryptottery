import { Injectable } from '@angular/core';
import {PersonApi} from "../../../bower_components/ng-cryptottery/sdk/services/custom/Person";
import * as _ from 'lodash';

@Injectable()
export class SessionProvider {
  public token:any = {};
  public currentPerson:any = {};
  // public host:string = 'http://159.89.184.132:8080';
  // public host:string = 'http://localhost:8080';
  public host:string = 'https://cryptottery.com';
  // public host:string = 'http://localhost:3000';
  public port:number = 80;
  // public hostUrl:string = this.host + ':' + this.port;
  public hostUrl:string = this.host;
  public coins:any = {
    // 'electroneum': {
    //   name: 'Electroneum',
    //   code: 'electroneum',
    //   abrv: 'ETN'
    // },
    // 'monero': {
    //   name: 'Monero',
    //   code: 'monero',
    //   abrv: 'XMR'
    // }
  };
  public coinsArr:any;
  public pools: any = {};
  public currentCoin:any = null;
  public currentPool:any = null;


  constructor(private Person:PersonApi) {
    console.log("----------------");
    console.log("host: ",this.host);
    console.log("----------------");
  }

  setCoins(coins) {
    this.coins = {};
    this.coinsArr = coins;
    for (let i = 0; i < coins.length; i++){
      let coin = coins[i];
      this.coins[coin.name] = coin;
    }
  }

  setCoin(coinCode){
    let c = this.coins[coinCode];
    if (c) {
      this.currentCoin = c;
      return c;
    } else {
      alert("Invalid Coin Code");
    }
  }

  login(credentials) {
    return new Promise((resolve,reject) => {
      if (!credentials.username) alert("Username is required!");
      if (!credentials.password) alert("Password is required!");

      this.Person.login(credentials,'user',credentials.rememberMe || false).subscribe(
        (tkn) => {
          this.token = tkn;
          this.getUser().then(
            (person) => {
              return resolve(person);
            }, err => {
              return reject(err);
            }
          );
        }, err => {
          console.log("Session login error ",err);
          return reject(err);
        }
      );
    });
  }

  register(credentials) {
    return new Promise((resolve,reject) => {
      if (!credentials.username) alert("Username is required!");
      if (!credentials.password) alert("Password is required!");

      this.Person.create(credentials)
        .subscribe(
          (tkn:any) => {
            this.login(credentials).then(
              (resp) => {
                return resolve(resp);
              }, (err) => {
                console.log("Session register error ",err);
                return reject(err);
              }
            );
            console.log("Token: ",tkn);
          }, (err) => {
            alert(err.message);
          }
        );
    });
  }

  getUser(){
    return new Promise((resolve,reject) => {
      this.Person.findById(this.token.userId).subscribe(
        (person) => {
          console.log("-----------");
          console.log(person);
          console.log("-----------");
          this.currentPerson = person;
          return resolve(this.currentPerson);
        }, err => {
          console.log("Session getuser error",err);
          return reject(err);
        }
      );
    });
  }
}
