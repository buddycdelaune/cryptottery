import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {LoginPage} from "../pages/login/login";
import {LoginComponent} from "../components/login/login";
import {RegisterComponent} from "../components/register/register";
import {LoginTypeComponent} from "../components/login-type/login-type";
import {SDKBrowserModule} from "../../bower_components/ng-cryptottery/sdk/index"
import { SessionProvider } from '../providers/session/session';
import {CryptoSpinnerComponent} from "../components/crypto-spinner/crypto-spinner";
import {AnonymousComponent} from "../components/anonymous/anonymous";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    LoginComponent,
    RegisterComponent,
    LoginTypeComponent,
    CryptoSpinnerComponent,
    AnonymousComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SDKBrowserModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LoginComponent,
    RegisterComponent,
    LoginTypeComponent,
    CryptoSpinnerComponent,
    AnonymousComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SessionProvider
  ]
})
export class AppModule {}
