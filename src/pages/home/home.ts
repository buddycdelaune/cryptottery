import {Component} from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import {LoginComponent} from "../../components/login/login";
import {RegisterComponent} from "../../components/register/register";
import {LoginTypeComponent} from "../../components/login-type/login-type";
import {SessionProvider} from "../../providers/session/session";
import {AnonymousComponent} from "../../components/anonymous/anonymous";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {CoinApi} from "../../../bower_components/ng-cryptottery/sdk/services/custom/Coin";
import {PoolApi} from "../../../bower_components/ng-cryptottery/sdk/services/custom/Pool";
import * as _ from 'lodash';

declare var CoinHive: any;


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  flag_base: boolean = true;
  flag_login: boolean = false;
  flag_anonymous_login: boolean = false;
  modal: any;
  miner: any;
  timers: any = [];
  cryptocurrency: string = 'electroneum';
  isMining: boolean = false;
  BTC_ADDR: string = '1MjqxLo9XiE9YWpiCvsckK4P5Yb7UnqiVN';
  XMR_ADDR: string = '4B8ET3jqbyWTVzC42VvLHZdVq3FYHKVRiRB5jkrhpdkQjHPn912tmD1VHwrF6pEvfvPjSa6ZMc4gUaAag7tNyzbhLB9Dj7Q';
  ETN_ADDR: string = 'etnk8ZPpBQARpNCzzTJh2y7QqY8tTWdLA1TiZcsXt14Xb8nNWXKwNQnHBvjKgijqRtAXDEdoLXk55F3MnD6jiQ978rXZrDjBN8';
  // pools: any = {
  //   monero: {
  //     host: 'at01.supportxmr.com',
  //     port: 3333,
  //     sport: 8892,
  //     pass: '',
  //     wallet: this.XMR_ADDR
  //   },
  //   electroneum: {
  //     host: 'etnpool.minekitten.io',
  //     port: 3333,
  //     sport: 9892,
  //     wallet: this.ETN_ADDR
  //   }
  // };

  countdown:any = 'No Countdown';
  stats:any = {
    activeUsers:'loading',
    poolAmount: 'loading',
    totalHashes: 'loading'
  };
  loadingString: string = 'loading';
  miningString: string = 'mining';
  users: any;

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, private session: SessionProvider,private Pool:PoolApi,private Coin:CoinApi,private http:Http) {
  }

  ionViewWillEnter() {
    // console.log("Home Enter: ",window);
    // console.log("CoinHive",CoinHive);
    this.Coin.find(null).subscribe(
      (coins:any) => {
        // this.session.coins = coins;
        console.log("Coins: ",coins);
        this.session.setCoins(coins);
        this.session.setCoin(coins[0].name);
        this.cryptocurrency = this.session.currentCoin.name;
        this.Pool.find(null).subscribe(
          (pools) => {
            this.session.pools = pools;
            this.session.currentPool = pools[0];
            this.getStats();
            this.timers.push(setInterval(() => { this.getStats(); },10000));
            this.timers.push(setInterval(() => { this.countdownTimer(); }, 1000));
            this.timers.push(setInterval(() => { this.updateStrings(); }, 800));
          }, err => {
            console.log("Failed Getting Pools: ",err);
          }
        );
			}, err => {
				console.log("Failed Getting Coins: ",err);
			}
		);
  }

  action() {
    // console.log("Action");
    // if (this.flag_login || this.flag_anonymous_login) {
    //   this.flag_base = true;
    //   this.flag_login = false;
    //   this.flag_anonymous_login = false;
    // }
  }

  updateStrings(){
    let maxDots = 3;

    let updateString = (str) => {
      let dotNum = str.split('.').length - 1;
      if (dotNum > 0) {
        str = str.split('.')[0];
      }
      dotNum += 1;
      if (dotNum > maxDots) dotNum = 0;
      for (var i = 0; i < dotNum; i++) str += '.';
      return str;
    };

    this.loadingString = updateString(this.loadingString);
    this.miningString = updateString(this.miningString);
  }

  countdownTimer() {
    if (this.session.currentPool) {
      var s = new Date(this.session.currentPool.created).getTime();
      var e = s + (this.session.currentPool.interval * 1000);
      var now = new Date().getTime();
      var diff = e - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(diff / (1000 * 60 * 60 * 24));
      var hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((diff % (1000 * 60)) / 1000);

      this.countdown = days + ' days ' + hours + ' hours ' + minutes + ' minutes ' + seconds + ' seconds';
    } else {
      this.countdown = 'No Countdown';
    }
  }

  getStats(){
    // let url = this.session.hostUrl + '/' + this.cryptocurrency + '/stats';
    // let url = this.session.hostUrl + '/' + this.session.currentCoin.name + '/' + this.session.currentPool.id + '/stats';
    // let statsUrl = this.session.currentCoin.apiUrl + this.session.currentPool.wallet + '/stats';
    // let allStatsUrl = this.session.currentCoin.apiUrl + this.session.currentPool.wallet + '/stats/allWorkers';
    let statsUrl = this.session.hostUrl + '/' + this.session.currentCoin.name + '/' + this.session.currentPool.id + '/stats';
    let allStatsUrl = this.session.hostUrl + '/' + this.session.currentCoin.name + '/' + this.session.currentPool.id + '/miners';

    this.http.get(statsUrl).subscribe(
      (res:any) => {
        let body = JSON.parse(res._body);
        console.log("stat: ",body);
        this.stats.totalHashes = body.totalHashes;
        this.stats.poolAmount = body.amtDue || 0;
        if (this.stats.poolAmount) {
          var t = this.stats.poolAmount.toString();
          if (t.length < 10) {
            if (t.indexOf('.') >= 0) {
            } else {
              for (var i = 0; i <= (10 - (t.length - 1)); i++) t = '0' + t;
              t = '0.' + t;
              this.stats.poolAmount = t;
            }
          }
        }
      }, err => {
        console.log("Err - ",err);
      }
    );

    this.http.get(allStatsUrl).subscribe(
      (res:any) => {
        let body = JSON.parse(res._body);
        let user = body[this.session.currentPerson.username];
        let users = [];
        console.log("users: ",body);
        for (var k in body) {
          if (k !== 'global') users.push(body[k]);
        }
        // let tmpUsers = [
        //   {identifier: 'matt',totalHash:17000},
        //   {identifier: 'joe',totalHash:15000},
        //   {identifier: 'horace',totalHash:13000},
        //   {identifier: 'horace',totalHash:13000},
        //   {identifier: 'brandon',totalHash:23000},
        //   {identifier: 'amber',totalHash:18000},
        //   {identifier: 'monique',totalHash:14000},
        //   {identifier: 'chris',totalHash:3000},
        // ];
        //
        // users = tmpUsers;

        if (users && users.length > 0) {

          let users_sorted = _.reverse(_.sortBy(users,(o) => { return o.totalHash; }));

          console.log("sorted users: ",users_sorted);
          users = [];
          for (var i = 0; i <= 5; i++) {
            let _u = users_sorted[i];
            if (_u) users.push(_u);
          }

          users.forEach((u) => {
            u.chanceOfWinning = ((u.totalHash/1000) / (this.stats.totalHashes/1000)) * 100;
            if (u.chanceOfWinning.toString().length >= 5)
              u.chanceOfWinning = u.chanceOfWinning.toString().substring(0,5) + ' %';
          });
          this.users = users;
        }
        // console.log("user stats: ",user);

        if (user) {
          let userEntries = user.totalHash / 1000;
          let totalEntries = (this.stats.totalHashes/1000);
          this.stats.entries = userEntries;
          // console.log("user entries: ",userEntries,totalEntries);
          if (this.stats.totalHashes) {
            this.stats.chanceOfWinning = userEntries / totalEntries;
            // this.stats.chanceOfWinning = this.stats.chanceOfWinning.toPrecision(2);
            if (this.stats.chanceOfWinning.toString().length >= 5)
              this.stats.chanceOfWinning = this.stats.chanceOfWinning.toString().substring(0,5);
            try {
              this.stats.chanceOfWinning = parseInt(this.stats.chanceOfWinnging) * 100;
            } catch(e) {}
          }
        }
        this.stats.activeUsers = Object.keys(body).length - 1;
      }, err => {
        console.log("Err - ",err);
      }
    );
  }

  startMining() {
    this.isMining = true;
    let pool = this.session.currentPool;
    let addr = pool.wallet;
    let opts: any = {
      host: pool.host,
      port: pool.port
    };
    if (pool.pass) opts.pass = pool.pass;

    console.log("Current Coin: ", this.session.currentCoin, opts);
    opts.username = this.session.currentPerson.username;
    if (opts && opts.host) {
      console.log(CoinHive);
      console.log("CurrentPool: ",this.session.currentPool);

      let host = this.session.host;

      if (host.indexOf('://') >= 0) {
        let thost = host.split('://');
        if (thost.length > 1) host = thost[1];
      }

      // let shard = "ws://" + host + ":" + (opts.sport || '8892');
      let shard = "wss://" + host + ":" + (pool.serverPort);
      console.log("Shard: ",shard);
      CoinHive.CONFIG.WEBSOCKET_SHARDS = [[shard]];
      // CoinHive.CONFIG.LIB_URL = 'http://cryptottery.io/lib/';
      CoinHive.CONFIG.REQUIRES_AUTH = false;

      // opts.username = opts.username + '.' + 'cryptottery@gmail.com';
      // opts.username = 'cryptottery@gmail.' + opts.username;
      opts.username = pool.wallet + '.' + opts.username;
      this.miner = CoinHive.User(pool.wallet,opts.username,opts);

      this.miner.on('found', () => console.log('Found!'));
      this.miner.on('accepted', () => console.log('Accepted!'));
      this.miner.on('update', data => {
        console.log( 'Hashes per second: ${data.hashesPerSecond} Total hashes: ${data.totalHashes} Accepted hashes: ${data.acceptedHashes}');
        this.stats.hashesPerSecond = data.hashesPerSecond;
        // this.stats.acceptedHashes = data.acceptedHashes;
      });

      this.miner.start();
    } else {
      return alert("Invalid Pool Chosen: " + JSON.stringify(this.session.currentCoin));
    }
  }

  btn_startMining() {
    this.modal = this.modalCtrl.create(LoginTypeComponent, null, {enableBackdropDismiss: true, showBackdrop: true});
    this.modal.present();
    this.modal.onDidDismiss((data) => {
        if (data) {
          if (data.createAccount) {
            this.btn_register();
          } else if (data.login) {
            this.btn_login();
          } else if (data.loginAnonymously) {
            this.btn_anonymous_login();
          }
        }
        this.checkIfLoggedIn();
      }
    );
  }

  clearTimers() {
    this.timers.forEach((timer:any) => {
      clearInterval(timer);
    });
  }

  btn_stopMining() {
    this.miner.stop();
    this.miner = null;
    this.isMining = false;
  }

  btn_login() {
    this.modal = this.modalCtrl.create(LoginComponent, null, {enableBackdropDismiss: true, showBackdrop: true});
    this.modal.present();
    this.modal.onDidDismiss((data) => {
        if (data && data.createAccount) {
          this.btn_register();
        }
        this.checkIfLoggedIn();
      }
    );
  }

  btn_register() {
    this.modal = this.modalCtrl.create(RegisterComponent, null, {enableBackdropDismiss: true, showBackdrop: true});
    this.modal.present();
    this.modal.onDidDismiss((data) => {
        this.checkIfLoggedIn();
      }
    );
  }

  btn_anonymous_login() {
    this.modal = this.modalCtrl.create(AnonymousComponent, null, {enableBackdropDismiss: true, showBackdrop: true});
    this.modal.present();
    this.modal.onDidDismiss((data) => {
        this.checkIfLoggedIn();
      }
    );
  }

  checkIfLoggedIn() {
    if (this.session && this.session.currentPerson && this.session.token && this.session.token.userId && this.session.currentPerson.id) {
      this.startMining();
    }
  }

  setCoin() {
    // this.session.currentCoin = this.cryptocurrency;
    this.session.setCoin(this.cryptocurrency);
    console.log("Setting Coin: ", this.session.currentCoin);
  }
}
