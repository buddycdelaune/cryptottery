import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";
import {Observable} from "rxjs";
import {SessionProvider} from "../../providers/session/session";

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginComponent {

  backdropHit:boolean = false;
  fordropHit:boolean = false;
  username:string = null;
  password:string = null;
  viewPass:boolean = false;
  rememberMe:boolean = true;

  constructor(private viewCtrl:ViewController,private session:SessionProvider) {
  }

  hitLBackdrop(){
    let hb = () => {
      if (!this.fordropHit) this.viewCtrl.dismiss();
    };
    Observable.timer(50).subscribe(hb);
  }

  hitLFordrop(){
    this.fordropHit = true;
    let hf = () => {
      this.fordropHit = false;
    };
    Observable.timer(150).subscribe(hf);
  }

  createAccount(){
    let vcd = () => {
      this.viewCtrl.dismiss({
        createAccount:true
      });
    };
    Observable.timer(155).subscribe(vcd);
  }

  loginL(){
    this.session.login({username:this.username,password:this.password,rememberMe:this.rememberMe}).then(
      (resp) => {
        this.viewCtrl.dismiss({
          loggedIn:true
        });
      }, err => {
        console.log("Login - login err",err);
      }
    )
  }
}
