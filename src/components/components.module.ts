import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login';
import { RegisterComponent } from './register/register';
import { LoginTypeComponent } from './login-type/login-type';
import { CryptoSpinnerComponent } from './crypto-spinner/crypto-spinner';
import { AnonymousComponent } from './anonymous/anonymous';
@NgModule({
	declarations: [LoginComponent,
    RegisterComponent,
    LoginTypeComponent,
    CryptoSpinnerComponent,
    AnonymousComponent],
	imports: [],
	exports: [LoginComponent,
    RegisterComponent,
    LoginTypeComponent,
    CryptoSpinnerComponent,
    AnonymousComponent]
})
export class ComponentsModule {}
