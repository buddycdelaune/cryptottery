import { Component } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {ViewController} from "ionic-angular";
import {PersonApi} from "../../../bower_components/ng-cryptottery/sdk/services/custom/Person";
import {SessionProvider} from "../../providers/session/session";
import {UUID} from 'angular2-uuid';

@Component({
  selector: 'anonymous',
  templateUrl: 'anonymous.html'
})
export class AnonymousComponent {

  backdropHit:boolean = false;
  fordropHit:boolean = false;
  viewPass:boolean = false;
  rememberMe:boolean = true;
  tos:boolean = false;
  twentyOne:boolean = false;
  username:string = null;
  email:string = null;
  password:string = null;
  electroneumWallet:string = null;
  moneroWallet:string = null;

  constructor(private viewCtrl:ViewController,private Person:PersonApi,private session:SessionProvider) {
  }

  hitABackdrop(){
    let hb = () => {
      if (!this.fordropHit) this.viewCtrl.dismiss();
    };
    Observable.timer(50).subscribe(hb);
  }

  hitAFordrop(){
    this.fordropHit = true;
    let hf = () => {
      this.fordropHit = false;
    };
    Observable.timer(150).subscribe(hf);
  }

  dismiss(){
    let vcd = () => {
      this.viewCtrl.dismiss({
      });
    };
    Observable.timer(155).subscribe(vcd);
  }

  anonymousRegister(){
    let credentials:any = {
      username: UUID.UUID(),
      password: UUID.UUID(),
      rememberMe: false
    };

    if (!credentials.username) return alert("Username is required!");
    if (!credentials.password) return alert("Password is required!");

    // if (!this.tos) return alert("You must agree to the terms of service to use Cryptottery!");
    // if (!this.twentyOne) return alert("You must be at least 21 years of age to use Cryptottery!");
    // if (!this.electroneumWallet && !this.moneroWallet) return alert("You must enter at least one wallet address for the coin you would like to mine!");

    this.session.register(credentials)
      .then(
        (tkn:any) => {
          this.dismiss();
        }, (err) => {
          alert(err.message);
        }
      );
  }
}
