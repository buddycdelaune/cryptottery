import { Component } from '@angular/core';
import {Observable} from "rxjs";
import {ViewController} from "ionic-angular";

@Component({
  selector: 'login-type',
  templateUrl: 'login-type.html'
})
export class LoginTypeComponent {

  backdropHit:boolean = false;
  fordropHit:boolean = false;

  constructor(private viewCtrl:ViewController) { }

  hitLTBackdrop(){
    let hb = () => {
      if (!this.fordropHit) this.viewCtrl.dismiss();
    };
    Observable.timer(50).subscribe(hb);
  }

  hitLTFordrop(){
    this.fordropHit = true;
    let hf = () => {
      this.fordropHit = false;
    };
    Observable.timer(150).subscribe(hf);
  }

  selection(type){
    let data = {
      createAccount:false,
      login:false,
      loginAnonymously:false
    };
    if (data.hasOwnProperty(type)) {
      data[type] = true;
    }
    let vcd = () => {
      this.viewCtrl.dismiss(data);
    };
    Observable.timer(155).subscribe(vcd);
  }
}
